package sda.Booksy;

public class Books {
    //0id,cat,name,price,inStock,author_t,series_t,sequence_i,genre_s
    private String index;
    private String category;
    private String name;
    private double price;
    private boolean inStock;
    private String authorT;
    private String seriesT;
    private int sequenceI;
    private String genre;

    public String getIndex() {
        return index;
    }

    public void setIndex(String index) {
        this.index = index;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public boolean isInStock() {
        return inStock;
    }

    public void setInStock(boolean inStock) {
        this.inStock = inStock;
    }

    public String getAuthorT() {
        return authorT;
    }

    public void setAuthorT(String authorT) {
        this.authorT = authorT;
    }

    public String getSeriesT() {
        return seriesT;
    }

    public void setSeriesT(String seriesT) {
        this.seriesT = seriesT;
    }

    public int getSequenceI() {
        return sequenceI;
    }

    public void setSequenceI(int sequenceI) {
        this.sequenceI = sequenceI;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public Books(String index, String category, String name, double price, boolean inStock, String authorT, String seriesT, int sequenceI, String genre) {
        this.index = index;
        this.category = category;
        this.name = name;
        this.price = price;
        this.inStock = inStock;
        this.authorT = authorT;
        this.seriesT = seriesT;
        this.sequenceI = sequenceI;
        this.genre = genre;

    }
}
